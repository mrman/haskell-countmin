{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Types where

import RIO
import Data.Bits (shiftL)
import qualified RIO.ByteString as B
import RIO.Text as T

-- | Helper function to convert bytestrings to integers
-- (borrowed from https://hackage.haskell.org/package/crypto-api-0.13.3)
bs2i :: B.ByteString -> Integer
bs2i = B.foldl' (\i b -> (i `shiftL` 8) + fromIntegral b) 0
{-# INLINE bs2i #-}

-- | Alias for hash functions that are usable in a CountMin sketch
type BSHashFn = B.ByteString -> Integer

-- | For use internally in CountMinSketch implementations
data HashMeta = HM { hmName :: !Text
                   , hmFn   :: !BSHashFn
                   }

-- | EQ instance for HashMeta that does nothing more than hash names
--   ideally this should also check initialization vectors/other parameters if a hash allows it
instance Eq HashMeta where
    (HM n1 _) == (HM n2 _) = n1 == n2

-- | EQ instance for HashMeta is just the name of the hash
instance Show HashMeta where
    show = T.unpack . hmName

-- | Typeclass that captures minimal set of functionality for a CountMin sketch implementation
class CountMin sketch where
    -- | Increment the count of a given key
    increment :: sketch -> B.ByteString -> sketch

    -- | Helper for incrementing with a regular string
    incrementStr :: sketch -> String -> sketch
    incrementStr s = increment s . encodeUtf8 . T.pack

    -- | Get the count of items seen for a given key
    count :: sketch -> B.ByteString -> Integer

    -- | Retrieve the hash functions in use
    hashFunctions :: sketch -> [HashMeta]

    -- | Retrieve the total count of events that this sketch has seen
    totalCount :: sketch -> Integer

-- | Dynamic sketches allow for re-configuration of hash functions at any time
class CountMin sketch => DynamicCountMin sketch where
    -- | Add a hash function for use with sketch
    addHashMeta :: sketch -> HashMeta -> sketch

    -- | Remove a hash function for use with the sketch
    removeHashMeta :: sketch -> HashMeta -> sketch

-- | Maximum performance sketches that reside in some monad m (likely IO)
class CountMinM m sketch where
    -- | Increment the count of a given key
    incrementM :: sketch -> ByteString -> m ()

    -- | Helper for incrementing with a regular string
    incrementStrM :: sketch -> String -> m ()
    incrementStrM s = incrementM s . encodeUtf8 . T.pack

    -- | Get the count of items seen for a given key
    countM :: sketch -> ByteString -> m Integer

    -- | Retrieve the hash functions in use
    hashFunctionsM :: sketch -> m [HashMeta]

    -- | Retrieve the total count of events that this sketch has seen
    totalCountM :: sketch -> m Integer

-- | Maximum performance dynamic sketches which allow for re-configuration of hash functions at any time
class CountMinM m sketch => DynamicCountMinM m sketch where
    -- | Add a hash function for use with sketch
    addHashMetaM :: sketch -> HashMeta -> m sketch

    -- | Remove a hash function for use with the sketch
    removeHashMetaM :: sketch -> HashMeta -> m sketch
