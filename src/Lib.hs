{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE LambdaCase #-}

module Lib ( buildSketch
           , buildNotASketch
           , buildBetterSketch
           , buildBestSketch
           , Sketch
           , NotASketch
           , BetterSketch
           , BestSketch
           , CountMin(..)
           , DynamicCountMin(..)
           , CountMinM(..)
           , DynamicCountMinM(..)
           ) where

import RIO
import Types
import qualified RIO.List as L
import qualified RIO.HashMap as HM
import qualified RIO.Vector.Boxed as VB
import qualified Data.HashTable.IO as HTIO

-- The sketch
data Sketch = Sketch { sHashFns :: ![HashMeta]
                     , sValues  :: !(VB.Vector (HashMap Integer Integer))
                     , sEventsProcessed :: !Integer
                     } deriving Show

-- | Create a sketch with the given functions and an empty vector of maps for the hash outputs
--
-- Examples:
--
-- >>> buildSketch []
-- Nothing
--
-- >>> import Hashes.FarmHash (hFarmHash)
-- >>> isJust $ buildSketch [hFarmHash]
-- True
buildSketch :: [HashMeta] -> Maybe Sketch
buildSketch [] = Nothing
buildSketch fns = Just $ Sketch fns (VB.replicate (length fns) HM.empty) 0

min2 :: Integer -> Integer -> Integer
min2 a b = if a < b then a else b

-- | Implement CountMin for sketch
instance CountMin Sketch where
    increment (Sketch fns maps ep)  key = Sketch fns updatedValues (ep + 1)
        where
          functions = VB.fromList $ hmFn <$> fns

          -- Fold over the list of functions, keeping track of minimums seen from previous functions' map data,
          -- producing an entirely new vector of modified hashmaps (very inefficient)
          (_, updatedValues) = VB.ifoldl' updateMinAndVector (-1, VB.empty) functions

          updateMinAndVector (prevMin, newVector) idx f =
              case maps VB.!? idx of
                -- Should never reach this case, since # of fns is the same as # of hashmaps (one per hashmap)
                Nothing     -> error "hashmap count does not match hash function count"
                -- Every case
                Just oldMap -> let hashedKey = f key
                               in case HM.lookup hashedKey oldMap of
                                    -- No count in the hashmap for this key yet
                                    Nothing -> if prevMin == (-1)
                                               -- No entry in map, prevMin @ orig value -1 means value must be new
                                               -- This relies on the fact that older functions (which should have values)
                                               -- are all processed *before* newer ones that won't have values
                                               -- NOTE: prevMin is set to 1 because we *know* at least one has been seen now for the current hash fn
                                               then (1, VB.snoc newVector (HM.insert hashedKey 1 oldMap))
                                               -- Case that we've encountered a new hash with no data yet
                                               -- We want to use the smallest value from other hashing functions
                                               else (prevMin, VB.snoc newVector (HM.insert hashedKey prevMin oldMap))
                                    -- A count is already being maintained, so use minimum of previous min and count going forward
                                    -- (if some hash function has a lower count we should use that when we get to new functions)
                                    Just existingCount -> ( if prevMin == -1 then existingCount else min2 prevMin existingCount
                                                          , VB.snoc newVector (HM.insert hashedKey (existingCount+1) oldMap)
                                                          )

    count s key = getMin functions []
        where
          functions = hmFn <$> sHashFns s

          -- Recursive function to do all hashing and retrieve min counts
          -- TODO: Should be possible to boil this down to fold + logic
          getMin []      acc = fromMaybe 0 (L.minimumMaybe acc)
          getMin (f:xs)  acc = getMin xs $ acc ++ [v]
              where
                hash = f key
                idx = length acc
                -- Get the hash map (if present) for the current hash fn by index
                v = case sValues s VB.!? idx of
                      Nothing -> 0
                      -- Look up the hashed value in the map that was provided
                      Just hmap -> fromMaybe 0 (HM.lookup hash hmap)

    hashFunctions :: Sketch -> [HashMeta]
    hashFunctions = sHashFns

    totalCount :: Sketch -> Integer
    totalCount = sEventsProcessed

instance DynamicCountMin Sketch where
    addHashMeta s@(Sketch oldFns oldValues ep) newFn = s { sHashFns=oldFns <> [newFn]
                                                         , sValues=VB.snoc oldValues HM.empty
                                                         , sEventsProcessed=ep
                                                         }

    removeHashMeta s@(Sketch oldFns oldValues ep) hashToRemove = case L.elemIndex hashToRemove oldFns of
                                                                Nothing -> s
                                                                Just idx -> s { sHashFns=filter (hashToRemove==) oldFns
                                                                              , sValues=VB.take idx oldValues <> VB.drop (idx+1) oldValues
                                                                              , sEventsProcessed=ep
                                                                              }

-- | Constructor for building a non sketch
buildNotASketch :: NotASketch
buildNotASketch = NotASketch HM.empty 0

-- | A sketch that isn't one, but instead uses the naive just-put-the-value-in-a-map approach
data NotASketch = NotASketch { nasValues          :: !(HashMap ByteString Integer)
                             , nasEventsProcessed :: !Integer
                             }

instance CountMin NotASketch where
    increment :: NotASketch -> ByteString -> NotASketch
    increment (NotASketch oldValues nep) key = NotASketch { nasValues=HM.insertWith (+) key 1 oldValues
                                                          , nasEventsProcessed=nep+1
                                                          }

    count :: NotASketch -> ByteString -> Integer
    count s key = HM.lookupDefault 0 key (nasValues s)

    hashFunctions :: NotASketch -> [HashMeta]
    hashFunctions _ = []

    totalCount :: NotASketch -> Integer
    totalCount = nasEventsProcessed

-- | Constructor for building the better sketch
buildBetterSketch :: [HashMeta] -> Maybe BetterSketch
buildBetterSketch []  = Nothing
buildBetterSketch fns = Just $ BetterSketch fns HM.empty 0

type HashValueMap t = t Text (t Integer Integer)

-- | A sketch that isn't one, but instead uses the naive just-put-the-value-in-a-map approach
data BetterSketch = BetterSketch { bsHashFns         :: ![HashMeta]
                                 , bsValues          :: !(HashValueMap HashMap)
                                 , bsEventsProcessed :: !Integer
                                 }

-- -- No longer needed b/c better sketch doesn't use a list of hashes
-- -- | Process a hash meta to produce the name of the meta and the hashed value of some bytestring
-- processMeta :: ByteString -> HashMeta -> (Text, Integer)
-- processMeta k (HM n f) = (n, f k)

getValueOrZero :: ByteString -> HashValueMap HashMap -> HashMeta -> Integer
getValueOrZero key values (HM n f) = case HM.lookup n values of
                                       Nothing -> 0
                                       Just m -> fromMaybe 0 $ HM.lookup (f key) m

instance CountMin BetterSketch where
    increment :: BetterSketch -> ByteString -> BetterSketch
    increment (BetterSketch fns oldMap ep) key = BetterSketch { bsHashFns=fns
                                                              , bsValues=newMap
                                                              , bsEventsProcessed=ep+1
                                                              }
        where
          doInsertWithFn outerMap (HM hashName hashFn) = let hashedValue = hashFn key
                                                         -- first level is the hashName
                                                         in case HM.lookup hashName outerMap of
                                                              Nothing -> HM.insert hashName (HM.singleton hashedValue 1) outerMap
                                                              Just innerMap -> case HM.lookup hashedValue innerMap of
                                                                                 Nothing -> HM.insert hashName (HM.insert hashedValue 1 innerMap) outerMap
                                                                                 Just v -> HM.insert hashName (HM.insert hashedValue (v+1) innerMap) outerMap
          newMap = foldl' doInsertWithFn oldMap fns

    count :: BetterSketch -> ByteString -> Integer
    count (BetterSketch fns values _ ) key = fromMaybe 0 $ L.minimumMaybe counts
        where
          counts = getValueOrZero key values  <$> fns

    hashFunctions :: BetterSketch -> [HashMeta]
    hashFunctions = bsHashFns

    totalCount :: BetterSketch -> Integer
    totalCount = bsEventsProcessed

instance DynamicCountMin BetterSketch where
    addHashMeta s@(BetterSketch oldMetas oldValues ep) newMeta@(HM hashName _) = s { bsHashFns=oldMetas <> [newMeta]
                                                                                   , bsValues=HM.insert hashName HM.empty oldValues
                                                                                   , bsEventsProcessed=ep
                                                                                   }

    removeHashMeta s@(BetterSketch oldFns oldValues ep) newMeta@(HM hashName _) = s { bsHashFns=filter (newMeta==) oldFns
                                                                                    , bsValues=HM.delete hashName oldValues
                                                                                    , bsEventsProcessed=ep
                                                                                    }


-- | Constructor for building the best sketch
buildBestSketch :: [HashMeta] -> IO (Maybe BestSketch)
buildBestSketch []  = pure Nothing
buildBestSketch fns = HTIO.new
                      >>= \newMap -> newIORef 0
                      >>= \ep -> pure $ Just $ BestSketch fns newMap ep

-- type HashTablesValueMap = H.BasicHashTable Text (HashMap Integer Integer)
type IOHashTable k v = HTIO.CuckooHashTable k v
type IOHashValueMap = IOHashTable Text (IOHashTable Integer Integer)

-- | A sketch that isn't one, but instead uses the naive just-put-the-value-in-a-map approach
data BestSketch = BestSketch { bstHashFns         :: ![HashMeta]
                             , bstValues          :: !IOHashValueMap
                             , bstEventsProcessed :: !(IORef Integer)
                             }

instance CountMinM IO BestSketch where
    incrementM :: BestSketch -> ByteString -> IO ()
    incrementM (BestSketch fns oldMap ep) key = mapM_ doMutation fns
                                                >> modifyIORef' ep (+1)
        where
          doMutation (HM hashName hashFn) = let hashedValue = hashFn key
                                            in HTIO.lookup oldMap hashName
                                               >>= \case
                                                   Nothing -> HTIO.fromList [(hashedValue, 1)]
                                                              >>= \createdMap -> HTIO.insert oldMap hashName createdMap
                                                   Just innerMap -> HTIO.lookup innerMap hashedValue
                                                                    >>= \case
                                                                        Nothing -> HTIO.insert innerMap hashedValue 1
                                                                        Just v -> HTIO.insert innerMap hashedValue (v+1)

    countM :: BestSketch -> ByteString -> IO Integer
    countM (BestSketch fns values _ ) key = mapM lookup2 fns
                                            >>= pure . catMaybes
                                            >>= pure . fromMaybe 0 . L.minimumMaybe
        where
          lookup2 (HM hashName hashFn) = HTIO.lookup values hashName
                                         >>= \case
                                             Nothing -> pure Nothing
                                             Just innerMap -> HTIO.lookup innerMap (hashFn key)

    hashFunctionsM :: BestSketch -> IO [HashMeta]
    hashFunctionsM = pure . bstHashFns

    totalCountM :: BestSketch -> IO Integer
    totalCountM = readIORef . bstEventsProcessed

instance DynamicCountMinM IO BestSketch where
    addHashMetaM s@(BestSketch oldMetas oldValues ep) newMeta@(HM hashName _) = HTIO.new
                                                                                >>= \newMap -> HTIO.insert oldValues hashName newMap
                                                                                >> pure (s { bstHashFns=oldMetas <> [newMeta]
                                                                                           , bstValues=oldValues
                                                                                           , bstEventsProcessed=ep
                                                                                           })

    removeHashMetaM s@(BestSketch oldFns oldValues ep) newMeta@(HM hashName _) = HTIO.delete oldValues hashName
                                                                                 >> pure (s { bstHashFns=filter (newMeta==) oldFns
                                                                                            , bstValues=oldValues
                                                                                            , bstEventsProcessed=ep
                                                                                            })
