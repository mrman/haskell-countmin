{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- | A AlwaysZero hash implementation
module Hashes.AlwaysZero (hAlwaysZero) where

import RIO
import Types (HashMeta(..))

-- | Hash function that performs AlwaysZero
hAlwaysZero :: HashMeta
hAlwaysZero = HM "AlwaysZero" (const 0)
