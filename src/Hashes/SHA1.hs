{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- | A SHA1 hash implementation
module Hashes.SHA1 (hSHA1) where

import RIO
import RIO.ByteString.Lazy (fromStrict)
import Data.Digest.Pure.SHA (sha1, integerDigest)
import Types (HashMeta(..))

hSHA1 :: HashMeta
hSHA1 = HM "SHA1" (integerDigest . sha1 . fromStrict)
