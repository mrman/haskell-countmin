{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- | A MD5 hash implementation
module Hashes.MD5 (hMD5) where

import RIO
import RIO.ByteString.Lazy (fromStrict)
import Data.Digest.Pure.MD5 (md5, md5DigestBytes)
import Types (HashMeta(..), bs2i)

hMD5 :: HashMeta
hMD5 = HM "MD5" (bs2i . md5DigestBytes . md5 . fromStrict)
