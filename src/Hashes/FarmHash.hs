{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- | A FarmHash hash implementation
module Hashes.FarmHash (hFarmHash) where

import RIO

import FarmHash (fingerprint64)
import Types (HashMeta(..))

-- | Hash function that performs FarmHash
hFarmHash :: HashMeta
hFarmHash = HM "FarmHash" (toInteger . fingerprint64)
