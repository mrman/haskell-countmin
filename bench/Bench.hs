{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE RankNTypes #-}
import RIO

import Criterion.Main (defaultMain, bgroup, env, nfIO, bench)
import Data.Maybe (fromJust)
import Weigh (mainWith, action)
import Control.DeepSeq (deepseq)
import System.Mem (performGC)

import Hashes.FarmHash (hFarmHash)
import Hashes.MD5 (hMD5)
import Hashes.SHA1 (hSHA1)
import Hashes.AlwaysZero (hAlwaysZero)
import Lib (buildSketch, buildBetterSketch, buildBestSketch, buildNotASketch, NotASketch, Sketch, BetterSketch, BestSketch, CountMin(..), CountMinM(..))
import BenchData (genRandomByteStrings)

-- | Control Sketch that with a hash function that does nothing
onlyAlwaysZero :: Sketch
onlyAlwaysZero = fromJust $ buildSketch [hAlwaysZero]

-- | BetterSketch with a hash function that does nothing
onlyAlwaysZeroBetter :: BetterSketch
onlyAlwaysZeroBetter = fromJust $ buildBetterSketch [hAlwaysZero]

-- | BestSketch with a hash function that does nothing
onlyAlwaysZeroBest :: IO BestSketch
onlyAlwaysZeroBest = fromJust <$> buildBestSketch [hAlwaysZero]

-- | Only MD5
onlyMD5 :: Sketch
onlyMD5 = fromJust $ buildSketch [hMD5]

-- | Only MD5 (BetterSketch)
onlyMD5Better :: BetterSketch
onlyMD5Better = fromJust $ buildBetterSketch [hMD5]

-- | Only MD5 (BestSketch)
onlyMD5Best :: IO BestSketch
onlyMD5Best = fromJust <$> buildBestSketch [hMD5]

-- | Only SHA1
onlySHA1 :: Sketch
onlySHA1 = fromJust $ buildSketch [hSHA1]

-- | Only SHA1 (BetterSketch)
onlySHA1Better :: BetterSketch
onlySHA1Better = fromJust $ buildBetterSketch [hSHA1]

-- | Only SHA1 (BestSketch)
onlySHA1Best :: IO BestSketch
onlySHA1Best = fromJust <$> buildBestSketch [hSHA1]

-- | Only FarmHash
onlyFarmHash :: Sketch
onlyFarmHash = fromJust $ buildSketch [hFarmHash]

-- | Only FarmHash (BetterSketch)
onlyFarmHashBetter :: BetterSketch
onlyFarmHashBetter = fromJust $ buildBetterSketch [hFarmHash]

-- | Only FarmHash (BestSketch)
onlyFarmHashBest :: IO BestSketch
onlyFarmHashBest = fromJust <$> buildBestSketch [hFarmHash]

-- | All available hashes (except for AlwaysZero)
allHashes :: Sketch
allHashes = fromJust $ buildSketch [hMD5, hFarmHash, hSHA1]

-- | All available hashes (except for AlwaysZero, BetterSketch)
allHashesBetter :: BetterSketch
allHashesBetter = fromJust $ buildBetterSketch [hMD5, hFarmHash, hSHA1]

-- | All available hashes (except for AlwaysZero, BestSketch)
allHashesBest :: IO BestSketch
allHashesBest = fromJust <$> buildBestSketch [hMD5, hFarmHash, hSHA1]

-- | A sketch that uses the naive approach of just storing every bytestring and a count in a map
notASketch :: NotASketch
notASketch = buildNotASketch

-- | Speed tests for various sketches
testSpeed :: IO ()
testSpeed = defaultMain
       [ env (genRandomByteStrings 1000) $ \ ~(values, _) ->
             bgroup "counting random strings (1000) " [ bench "w/ Sketch (AlwaysZero)" $ nfIO $ pure $ totalCount (foldl' increment onlyAlwaysZero values)
                                                      , bench "w/ BetterSketch (AlwaysZero)" $ nfIO $ pure $ totalCount (foldl' increment onlyAlwaysZeroBetter values)
                                                      , bench "w/ BestSketch (AlwaysZero)" $ nfIO $ (onlyAlwaysZeroBest
                                                                                                     >>= \s -> mapM_ (incrementM s) values
                                                                                                     >> totalCountM s)

                                                      , bench "w/ Sketch (MD5)" $ nfIO $ pure $ totalCount (foldl' increment onlyMD5 values)
                                                      , bench "w/ BetterSketch (MD5)" $ nfIO $ pure $ totalCount (foldl' increment onlyMD5Better values)
                                                      , bench "w/ BestSketch (MD5)" $ nfIO $ (onlyMD5Best
                                                                                              >>= \s -> mapM_ (incrementM s) values
                                                                                              >> totalCountM s)

                                                      , bench "w/ Sketch (FarmHash)" $ nfIO $ pure $ totalCount (foldl' increment onlyFarmHash values)
                                                      , bench "w/ BetterSketch (FarmHash)" $ nfIO $ pure $ totalCount (foldl' increment onlyFarmHashBetter values)
                                                      , bench "w/ BestSketch (FarmHash)" $ nfIO $ (onlyFarmHashBest
                                                                                                   >>= \s -> mapM_ (incrementM s) values
                                                                                                   >> totalCountM s)

                                                      , bench "w/ Sketch (SHA1)" $ nfIO $ pure $ totalCount (foldl' increment onlySHA1 values)
                                                      , bench "w/ BetterSketch (SHA1)" $ nfIO $ pure $ totalCount (foldl' increment onlySHA1Better values)
                                                      , bench "w/ BestSketch (SHA1)" $ nfIO $ (onlySHA1Best
                                                                                               >>= \s -> mapM_ (incrementM s) values
                                                                                               >> totalCountM s)

                                                      , bench "w/ Sketch (MD5, FarmHash, SHA1)" $ nfIO $ pure $ totalCount (foldl' increment allHashes values)
                                                      , bench "w/ BetterSketch (MD5, FarmHash, SHA1)" $ nfIO $ pure $ totalCount (foldl' increment allHashesBetter values)
                                                      , bench "w/ BestSketch (MD5, FarmHash, SHA1)" $ nfIO $ (allHashesBest
                                                                                                              >>= \s -> mapM_ (incrementM s) values
                                                                                                              >> totalCountM s)
                                                      ]
       , env (genRandomByteStrings 100_000) $ \ ~(values, _) ->
           bgroup "counting random strings (100,000) " [ bench "w/ Sketch (AlwaysZero)" $ nfIO $ pure $ totalCount (foldl' increment onlyAlwaysZero values)
                                                       , bench "w/ BetterSketch (AlwaysZero)" $ nfIO $ pure $ totalCount (foldl' increment onlyAlwaysZeroBetter values)
                                                       , bench "w/ BestSketch (AlwaysZero)" $ nfIO $ (onlyAlwaysZeroBest
                                                                                                      >>= \s -> mapM_ (incrementM s) values
                                                                                                      >> totalCountM s)

                                                       , bench "w/ Sketch (MD5)" $ nfIO $ pure $ totalCount (foldl' increment onlyMD5 values)
                                                       , bench "w/ BetterSketch (MD5)" $ nfIO $ pure $ totalCount (foldl' increment onlyMD5Better values)
                                                       , bench "w/ BestSketch (MD5)" $ nfIO $ (onlyMD5Best
                                                                                               >>= \s -> mapM_ (incrementM s) values
                                                                                               >> totalCountM s)

                                                       , bench "w/ Sketch (FarmHash)" $ nfIO $ pure $ totalCount (foldl' increment onlyFarmHash values)
                                                       , bench "w/ BetterSketch (FarmHash)" $ nfIO $ pure $ totalCount (foldl' increment onlyFarmHashBetter values)
                                                       , bench "w/ BestSketch (FarmHash)" $ nfIO $ (onlyFarmHashBest
                                                                                                    >>= \s -> mapM_ (incrementM s) values
                                                                                                    >> totalCountM s)

                                                       , bench "w/ Sketch (SHA1)" $ nfIO $ pure $ totalCount (foldl' increment onlySHA1 values)
                                                       , bench "w/ BetterSketch (SHA1)" $ nfIO $ pure $ totalCount (foldl' increment onlySHA1Better values)
                                                       , bench "w/ BestSketch (SHA1)" $ nfIO $ (onlySHA1Best
                                                                                                >>= \s -> mapM_ (incrementM s) values
                                                                                                >> totalCountM s)

                                                       , bench "w/ Sketch (MD5, FarmHash, SHA1)" $ nfIO $ pure $ totalCount (foldl' increment allHashes values)
                                                       , bench "w/ BetterSketch (MD5, FarmHash, SHA1)" $ nfIO $ pure $ totalCount (foldl' increment allHashesBetter values)
                                                       , bench "w/ BestSketch (MD5, FarmHash, SHA1)" $ nfIO $ (allHashesBest
                                                                                                               >>= \s -> mapM_ (incrementM s) values
                                                                                                               >> totalCountM s)
                                                       ]
       ]

-- | Testing for memory usage while incrementing (allocations + GC)
testMemoryUsage :: IO ()
testMemoryUsage = sequence [genRandomByteStrings 1000, genRandomByteStrings 100_000]
                  >>= \[(v, _), (vv, _)] -> mainWith ( -- | 1000 increment tests
                                                      action "1000 increments (NotASketch)" (pure $ totalCount $ foldl' increment notASketch v)
                                                     >> action "1000 increments (Sketch)" (pure $ totalCount $ foldl' increment allHashes v)
                                                     >> action "1000 increments (BetterSketch)" (pure $ totalCount $ foldl' increment allHashesBetter v)
                                                     >> action "1000 increments (BestSketch)" (allHashesBest
                                                                                               >>= \s -> mapM_ (incrementM s) v
                                                                                               >> totalCountM s)

                                                     -- | 100,000 increment tests
                                                     >> action "100,000 increments (NotASketch)" (pure $ totalCount $ foldl' increment notASketch vv)
                                                     >> action "100,000 increments (Sketch)" (pure $ totalCount $ foldl' increment allHashes vv)
                                                     >> action "100,000 increments (BetterSketch)" (pure $ totalCount $ foldl' increment allHashesBetter vv)
                                                     >> action "100,000 increments (BestSketch)" (allHashesBest
                                                                                                  >>= \s -> mapM_ (incrementM s) vv
                                                                                                  >> totalCountM s)
                                                     )


-- -- | Uncomment to speed
-- main :: IO ()
-- main = putStrLn "=== Speed ===\n"
--        >> testSpeed

-- | Uncomment to test memory
main :: IO ()
main = putStrLn "=== Memory Usage ===\n"
       >> testMemoryUsage
