{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module BetterSketchSpec where

import RIO
import Lib (buildBetterSketch, BetterSketch, CountMin(..), DynamicCountMin(..))
import Data.Maybe (fromJust)
import Hashes.SHA1 (hSHA1)
import Hashes.FarmHash (hFarmHash)
import Hashes.MD5 (hMD5)
import Test.Hspec

-- | Fixture where 'test' shows up 3 times amongst other data
threeHits :: [String]
threeHits = ["test", "test2", "threeHits", "test", "test5", "test"]

spec :: Spec
spec = do
  describe "CountMin BetterSketch" $ do
         it "cannot be created with no hashing functions" $ isNothing (buildBetterSketch []) `shouldBe` True

  describe "CountMin Sketch w/ SHA1" $ do
         it "can be created with only sha1" $ isJust (buildBetterSketch [hSHA1]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildBetterSketch [hSHA1]) :: IO (Maybe BetterSketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "CountMin Sketch w/ MD5" $ do
         it "can be created with only md5" $ isJust (buildBetterSketch [hMD5]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildBetterSketch [hMD5]) :: IO (Maybe BetterSketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "CountMin Sketch w/ FarmHash" $ do
         it "can be created with only farmhash" $ isJust (buildBetterSketch [hFarmHash]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildBetterSketch [hFarmHash]) :: IO (Maybe BetterSketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "DynamicCountMin BetterSketch" $ do
         it "can add hash function (SHA1 + FarmHash)" $ \_ -> (pure (buildBetterSketch [hSHA1]))
                                                              >>= pure . fromJust
                                                              >>= \onlySHA -> pure (addHashMeta onlySHA hFarmHash)
                                                              >>= \updatedSketch -> length (hashFunctions updatedSketch) `shouldBe` 2

         it "generates a count properly (SHA1 + FarmHash)" $ \_ -> (pure (buildBetterSketch [hSHA1]))
                                                                   >>= pure . fromJust
                                                                   >>= \onlySHA -> pure (addHashMeta onlySHA hFarmHash)
                                                                   >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                                   -- Ensure the sketch produced *at least* 3
                                                                   >>= \s -> (count s "test" >= 3)`shouldBe` True
