{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Hashes.FarmHashSpec (spec) where

import RIO
import RIO.Text (encodeUtf8)
import Hashes.FarmHash (hFarmHash)
import Test.Hspec
import Types (BSHashFn, HashMeta(..))

farmHash :: BSHashFn
farmHash = hmFn hFarmHash

spec :: Spec
spec = do
  describe "FarmHash hash function" $ do
         -- | "count-min" ---utf8Encode--> utf8 bytes --farmhash + int conv--> 4631194986186915441
         it "produces the expected integer for 'count-min'" $ farmHash (encodeUtf8 "count-min") `shouldBe` 4631194986186915441
