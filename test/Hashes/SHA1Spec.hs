{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Hashes.SHA1Spec (spec) where

import RIO
import RIO.Text (encodeUtf8)
import Hashes.SHA1 (hSHA1)
import Test.Hspec
import Types (BSHashFn, HashMeta(..))

shaHash :: BSHashFn
shaHash = hmFn hSHA1

spec :: Spec
spec = do
  describe "SHA1 hash function" $ do
         -- | "count-min" ---utf8Encode--> utf8 bytes --sha1 + int conv--> 735099499418687102572016543064621013040670267903
         it "produces the expected integer for 'count-min'" $ shaHash (encodeUtf8 "count-min") `shouldBe` 735099499418687102572016543064621013040670267903
