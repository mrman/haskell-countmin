{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Hashes.MD5Spec (spec) where

import RIO
import RIO.Text (encodeUtf8)
import Hashes.MD5 (hMD5)
import Test.Hspec
import Types (BSHashFn, HashMeta(..))

shaHash :: BSHashFn
shaHash = hmFn hMD5

spec :: Spec
spec = do
  describe "MD5 hash function" $ do
         -- | "count-min" ---utf8Encode--> utf8 bytes --md5 & int conv--> 16897994170140391222779493685304851206
         it "produces the expected integer for 'count-min'" $ shaHash (encodeUtf8 "count-min") `shouldBe` 16897994170140391222779493685304851206
