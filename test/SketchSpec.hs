{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module SketchSpec where

import RIO
import Lib (buildSketch, Sketch, CountMin(..), DynamicCountMin(..))
import Data.Maybe (fromJust)
import Hashes.SHA1 (hSHA1)
import Hashes.FarmHash (hFarmHash)
import Hashes.MD5 (hMD5)
import Test.Hspec

-- | Fixture where 'test' shows up 3 times amongst other data
threeHits :: [String]
threeHits = ["test", "test2", "threeHits", "test", "test5", "test"]

spec :: Spec
spec = do
  describe "CountMin Sketch" $ do
         it "cannot be created with no hashing functions" $ isNothing (buildSketch []) `shouldBe` True

  describe "CountMin Sketch w/ SHA1" $ do
         it "can be created with only sha1" $ isJust (buildSketch [hSHA1]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildSketch [hSHA1]) :: IO (Maybe Sketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "CountMin Sketch w/ MD5" $ do
         it "can be created with only md5" $ isJust (buildSketch [hMD5]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildSketch [hMD5]) :: IO (Maybe Sketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "CountMin Sketch w/ FarmHash" $ do
         it "can be created with only farmhash" $ isJust (buildSketch [hFarmHash]) `shouldBe` True

         it "generates a count properly" $ \_ -> (pure (buildSketch [hFarmHash]) :: IO (Maybe Sketch))
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                 -- Ensure the sketch produced *at least* 3
                                                 >>= \s -> (count s "test" >= 3)`shouldBe` True

  describe "DynamicCountMin Sketch" $ do
         it "can add hash function (SHA1 + FarmHash)" $ \_ -> (pure (buildSketch [hSHA1]))
                                                              >>= pure . fromJust
                                                              >>= \onlySHA -> pure (addHashMeta onlySHA hFarmHash)
                                                              >>= \updatedSketch -> length (hashFunctions updatedSketch) `shouldBe` 2

         it "generates a count properly (SHA1 + FarmHash)" $ \_ -> (pure (buildSketch [hSHA1]))
                                                                   >>= pure . fromJust
                                                                   >>= \onlySHA -> pure (addHashMeta onlySHA hFarmHash)
                                                                   >>= \emptySketch -> pure (foldl' incrementStr emptySketch threeHits)
                                                                   -- Ensure the sketch produced *at least* 3
                                                                   >>= \s -> (count s "test" >= 3)`shouldBe` True
