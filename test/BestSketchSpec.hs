{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module BestSketchSpec where

import RIO
import Lib (buildBestSketch, CountMinM(..), DynamicCountMinM(..))
import Data.Maybe (fromJust)
import Hashes.SHA1 (hSHA1)
import Hashes.FarmHash (hFarmHash)
import Hashes.MD5 (hMD5)
import Test.Hspec

-- | Fixture where 'test' shows up 3 times amongst other data
threeHits :: [String]
threeHits = ["test", "test2", "threeHits", "test", "test5", "test"]

spec :: Spec
spec = do
  describe "CountMinM BestSketch" $ do
         it "cannot be created with no hashing functions" $ \_ -> buildBestSketch []
                                                                  >>= shouldBe True . isNothing


  describe "CountMinM BestSketch w/ SHA1" $ do
         it "can be created with only sha1" $ \_ -> buildBestSketch [hSHA1]
                                                    >>= shouldBe True . isJust

         it "generates a countM properly" $ \_ -> buildBestSketch [hSHA1]
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \sketch -> mapM_ (incrementStrM sketch) threeHits
                                                 -- Ensure the sketch produced *at least* 3
                                                 >> countM sketch "test"
                                                 >>= shouldBe 3

  describe "CountMinM BestSketch w/ MD5" $ do
         it "can be created with only md5" $ \_ -> buildBestSketch [hMD5]
                                                   >>= shouldBe True . isJust

         it "generates a countM properly" $ \_ -> buildBestSketch [hMD5]
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \sketch -> mapM_ (incrementStrM sketch) threeHits
                                                 -- Ensure the sketch produced *at least* 3
                                                 >> countM sketch "test"
                                                 >>= shouldBe 3

  describe "CountMinM BestSketch w/ FarmHash" $ do
         it "can be created with only farmhash" $ \_ -> buildBestSketch [hFarmHash]
                                                        >>= shouldBe True . isJust

         it "generates a countM properly" $ \_ -> buildBestSketch [hFarmHash]
                                                 >>= pure . fromJust
                                                 -- Insert data into the sketch that should record 'test' 3 times
                                                 >>= \sketch -> mapM_ (incrementStrM sketch) threeHits
                                                 -- Ensure the sketch produced *at least* 3
                                                 >> countM sketch "test"
                                                 >>= shouldBe 3

  describe "DynamicCountMin BestSketch" $ do
         it "can add hash function (SHA1 + FarmHash)" $ \_ -> buildBestSketch [hSHA1]
                                                              >>= pure . fromJust
                                                              >>= \onlySHA -> addHashMetaM onlySHA hFarmHash
                                                              >>= hashFunctionsM
                                                              >>= shouldBe 2 . length

         it "generates a countM properly (SHA1 + FarmHash)" $ \_ -> buildBestSketch [hSHA1]
                                                                   >>= pure . fromJust
                                                                   >>= \sketch -> addHashMetaM sketch hFarmHash
                                                                   >> mapM_ (incrementStrM sketch) threeHits
                                                                   -- Ensure the sketch produced *at least* 3
                                                                   >> countM sketch "test"
                                                                   >>= shouldBe 3
