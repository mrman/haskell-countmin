# countmin #

`countmin` is a [haskell](https://www.haskell.org/) library enabling easy use of the [Count-min sketch](https://en.wikipedia.org/wiki/Count%E2%80%93min_sketch).

This repository was produced to go along with [a blog post I wrote](https://vadosware.io/post/countmin-sketch-in-haskell), so feel free to check it out for more context!
